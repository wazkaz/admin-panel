<?php

use Illuminate\Support\Facades\Route;
use App\Mail\CompanyAdded;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/alpha', function () {
    return view('alpha');
});

Route::get('/home/companies', 'CompaniesController@index')->name('companies.index');
Route::get('/home/companies/create', 'CompaniesController@createIndex')->name('companies.create');
Route::post('/home/companies/create/add', 'CompaniesController@store')->name('create.add');
Route::get('/home/companies/view/{name}', 'CompaniesController@viewIndex')->name('companies.view');
Route::get('/home/companies/update/{name}', 'CompaniesController@updateIndex')->name('companies.update');
Route::post('/home/companies/update/{name}/modify', 'CompaniesController@update');
Route::get('/home/comapnies/delete/{name}', 'CompaniesController@delete');


Route::get('/home/employees', 'EmployeesController@index')->name('employees.index');
Route::get('/home/employees/create', 'EmployeesController@createIndex')->name('employees.create');
Route::post('/home/employees/create/add', 'EmployeesController@store');
Route::get('/home/employees/view/{id}', 'EmployeesController@viewIndex')->name('employees.view');
Route::get('/home/employees/update/{id}', 'EmployeesController@updateIndex')->name('employees.update');
Route::post('/home/employees/update/{id}/modify', 'EmployeesController@update');
Route::get('/home/employees/delete/{id}', 'EmployeesController@delete');




Auth::routes([
    'register' => false,
]);

Route::get('/home', 'HomeController@index')->name('home');
