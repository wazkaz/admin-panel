<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Companies;
use Faker\Generator as Faker;

$factory->define(Companies::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->email,
        'logo' => $faker->image(public_path('img\logos'), 100, 100, null, false),
        // 'logo' => $faker->image($dir = storage_path('app\public\logos'), $width = 150, $height = 150),
        'website' => $faker->url,
    ];
});
