<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employees;
use App\Companies;
use Faker\Generator as Faker;

$factory->define(Employees::class, function (Faker $faker) {
    return [
        'fname' => $faker->firstName,
        'lname' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->numerify('##########'),
        'company' => factory(Companies::class),
    ];
});
