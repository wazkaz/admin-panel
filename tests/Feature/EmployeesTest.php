<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\User;
use App\Employees;
use App\Companies;

class EmployeesTest extends TestCase
{

    use RefreshDatabase;

    public function setUp() :void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function test_an_authenticated_user_can_see_employee_index_page ()
    {
        $response = $this->actingAs($this->user)
                        ->get('/home/employees');
        
        $response->assertStatus(200);
    }

    public function test_an_authenticated_user_can_see_employee_create_index_page ()
    {
        $response = $this->actingAs($this->user)
            ->get('/home/employees/create');
        
        $response->assertStatus(200);
    }

    public function test_an_authenticated_user_can_add_new_employee () 
    {
        $companies = factory(Companies::class)->create();
        $employee = factory(Employees::class)->create();


        $employees = $employee->companies()->associate($companies);
        $employee = $employees->toArray();
        $this->actingAs($this->user);
        $response = $this->postJson('/home/employees/create/add', $employee);
        $response->assertStatus(302);
        // dd($response->getContent());
    }

    public function test_an_authenticated_user_can_see_single_employee_page ()
    {
        $employee = factory(Employees::class)->create();

        $response = $this->actingAs($this->user)
                        ->call('GET', '/home/employees/view/'.$employee->id);
        $response->assertStatus(200);

    }

    public function test_an_authenticated_user_can_see_employee_update_page () 
    {
        $employee = factory(Employees::class)->create();

        $response = $this->actingAs($this->user)
                        ->call('GET', '/home/employees/update/'.$employee->id);
        $response->assertStatus(200);
    }

    public function test_an_authenticated_user_can_update_an_employee ()
    {
        $companies = factory(Companies::class)->create();
        $employee = factory(Employees::class)->create();

        $employeesToUpdate = factory(Employees::class)->create();

        $employeeToUpdate = $employeesToUpdate->toArray(); 

        $employees = $employee->companies()->associate($companies);
        $employee = $employees->toArray();
        $this->actingAs($this->user);
        $response = $this->postJson('/home/employees/update/'.$employees->id.'/modify', $employeeToUpdate);
        $response->assertStatus(302);
    }

    public function test_an_authenticated_user_can_delete_employee ()
    {
        $employee = factory(Employees::class)->create();

        $this->actingAs($this->user);

        $response = $this->actingAs($this->user)
                ->call('GET', '/home/employees/delete/'.$employee->id);
        $response->assertStatus(302);
    }
}
