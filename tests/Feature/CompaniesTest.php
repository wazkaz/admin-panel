<?php

namespace Tests\Feature;

use App\Notifications\OrderShipped;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;
use App\Notifications\CompanyNotification;
use Tests\TestCase;
use App\User;
use App\Companies;

class CompaniesTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() :void
    {
        parent::setUp();
        $this->user = factory(User::class)->make();
    }

    public function test_an_authenticated_user_can_see_companies_index_page ()
    {
        $response = $this->actingAs($this->user)
                        ->get('/home/companies');
        $response->assertStatus(200);
    }

    public function test_an_authenticated_user_can_see_companies_create_index_page ()
    {
        $response = $this->actingAs($this->user)
            ->get('/home/companies/create');
        
        $response->assertStatus(200);
    }

    public function test_an_authenticated_user_can_add_new_companie () 
    {
        Storage::fake('public');

        $image = UploadedFile::fake()->image('logo.jpg', 150, 150);

        $data = [
            'name' => 'RandomInc',
            'email' => 'random@random.com',
            'logo' => $image,
            'website' => 'http://www.google.com'
        ];
        $companyName = 'RandomUnCA.';
        $user = $this->user;
        $dataToSend = [
            'from' => $user->email,
            'greeting' => 'Greetings',
            'subject' => 'New company notification',
            'line1' => 'New company' . ' ' . $companyName . ' ' . 'added.',
            'line2' => 'Best regards!',
            'company' => $companyName
        ];
        Notification::fake();

        $user->notify(new CompanyNotification($dataToSend));

        Notification::assertSentTo(
            $user, 
            CompanyNotification::class
        );

        $this->actingAs($this->user);
        $response = $this->postJson('/home/companies/create/add', $data);
        // dd($response->getContent());
        $response->assertStatus(302);
        Storage::disk('public')->assertExists('img/logos/'.$image->hashName());
        Storage::disk('public')->assertMissing('missing.jpg');

        // dd(Notification::fake());
    }

    public function test_an_authenticated_user_can_see_single_company_page ()
    {
        $company = factory(Companies::class)->create();

        $response = $this->actingAs($this->user)
                        ->call('GET', '/home/companies/view/'.$company->name);
        $response->assertStatus(200);

    }

    public function test_an_authenticated_user_can_see_company_update_page () 
    {
        $company = factory(Companies::class)->create();

        $response = $this->actingAs($this->user)
                        ->call('GET', '/home/companies/update/'.$company->name);
        $response->assertStatus(200);
    }

    public function test_an_authenticated_user_can_update_a_company ()
    {
        Storage::fake('public');

        $imageToUpdate = UploadedFile::fake()->image('logo2.jpg', 100, 100);

        $dataToUpdate = [
            'name' => 'RandomKFT',
            'email' => 'randomized@rand.com',
            'logo' => $imageToUpdate,
            'website' => 'https://www.youtube.com/'
        ];

        $company = factory(Companies::class)->create();

        $this->actingAs($this->user);
        $response = $this->postJson('/home/companies/update/'.$company->name.'/modify', $dataToUpdate);
        $response->assertStatus(302);
        Storage::disk('public')->assertExists('img/logos/'.$imageToUpdate->hashName());
        Storage::disk('public')->assertMissing('missing.jpg');
    }

    public function test_an_authenticated_user_can_delete_company ()
    {
        $company = factory(Companies::class)->create();

        $this->actingAs($this->user);

        $response = $this->actingAs($this->user)
                ->call('GET', '/home/comapnies/delete/'.$company->name);
        $response->assertStatus(302);
    }
}
