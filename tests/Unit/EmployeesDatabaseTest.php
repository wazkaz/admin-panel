<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Employees;

class EmployeesDatabaseTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_company_name_is_recorded()
    {

        Employees::create([
            'fname' => 'John',
            'lname' => 'Doe',
            'name' => 'Random Inc.',
        ]);

        $this->assertCount(1, Employees::all());
    }
}
