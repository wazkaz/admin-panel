<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Companies;


class CompaniesDatabaseTest extends TestCase
{
    use RefreshDatabase;

    public function test_only_name_is_required_to_create_an_company()
    {
        Companies::firstOrCreate([
            'name' => 'Random Inc.',
        ]);

        $this->assertCount(1, Companies::all());
    }
}
