<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companies;
use App\User;
use App\Mail\CompanyAdded;
use App\Notifications\CompanyNotification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CompaniesController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {

        $companies = Companies::all();

        return view('companies.index', [
            'companies' => $companies
        ]);
    }

    public function createIndex() {
        
        return view('companies.create');
    }

    public function store(Request $request) {


        $validatedData = $request->validate([
            'name'              =>  ['required', 'unique:companies,name', 'string', 'max:55'],
            'email'             =>  ['nullable', 'unique:companies,email', 'string', 'email', 'max:255'],
            'website'           =>  ['nullable', 'string', 'url', 'max:255'],
            'logo'              =>  ['nullable', 'image', 'dimensions:min_width=100,min_height=100']
        ]);
        
        $companies = new Companies();

        $companies->name = $request->input('name');
        $companies->email = $request->input('email');
        $companies->website = $request->input('website');

        $file = $request->file('logo');

        if($file == null) {
            $companies->save();
        } else {
            $path = $file->store('img/logos', 'public');
            $companies->logo = $path;
            
            $file->store('img/logos', 'public');
            // Storage::putFileAs('logos/', new File($file), $filename);
    
            $companies->save();
        }
        // dd($file);

        /* Sending notification  */
        // $user = User::first();

        $email = $request->input('email');
        $companyName = $request->input('name');

        $data = [
            /* Valamiért bele köt a test az email properti-ben és nem tudom, hogy miért
                egyenlőre még nem kaptam megoldást rá */
            // 'from' => $user->email,
            'from' => 'admin@admin.com',
            'greeting' => 'Greetings',
            'subject' => 'New company notification',
            'line1' => 'New company' . ' ' . $companyName . ' ' . 'added.',
            'line2' => 'Best regards!',
            'company' => $companyName
        ];

        Notification::route('mail', $email)
                    ->notify(new CompanyNotification($data));

        /* Sending email  */

        // $data = $request->all();
        // $email = $request->input('email');
        // Mail::to($email)->send(new CompanyAdded($data));

        return redirect()->back()->with('mssg', 'Create successfull!');
    }

    public function viewIndex($nameId) {

        $company = Companies::findOrFail($nameId);
        return view('companies.view', [
            'company' => $company
        ]);
    }

    public function updateIndex($nameId) {

        $company = Companies::findOrFail($nameId);
        return view('companies.update', [
            'company' => $company
        ]);
    }

    public function update(Request $request, $nameId) {

        $validatedData = $request->validate([
            'name'              =>  ['required', 'string', 'max:55'],
            'email'             =>  ['nullable', 'string', 'email', 'max:255'],
            'website'           =>  ['nullable', 'url', 'max:255'],
            'logo'              =>  ['nullable', 'image', 'dimensions:min_width=100,min_height=100']
        ]);

        $company = Companies::findOrFail($nameId);

        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->website = $request->input('website');

        $file = $request->file('logo');

        if($file == null) {
            $company->save();
        } else {
            $path = $file->store('img/logos', 'public');
            $company->logo = $path;
            
            $file->store('img/logos', 'public');
    
            $company->save();
        }

        return redirect('/home/companies');
    }

    public function delete($nameId) {

        $company = Companies::findOrFail($nameId);

        if(file_exists($company->logo)) {
            unlink(public_path() . '/' . $company->logo );
            Storage::delete($company->logo);
            $company->delete();
    
            return redirect()->back()->with('alert', 'Company deleted!');
        } else {
            Storage::delete($company->logo);
            $company->delete();

            return redirect()->back()->with('alert', 'Company deleted!');
        }
    }
}
