<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employees;
use App\Companies;

class EmployeesController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $employees = Employees::all();

        return view('employees.index', [
            'employees' => $employees,
        ]);
    }

    public function createIndex() {

        $employees = Employees::all();
        $compnanies = Companies::all();

        return view('employees.create', [
            'employees' => $employees,
            'compnanies'=> $compnanies
        ]);
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'fname'   =>  ['required', 'string', 'alpha', 'max:55'],
            'lname'   =>  ['required', 'string', 'alpha', 'max:55'],
            'company' => ['present', 'string', 'max:100'],
            'email' => ['present', 'email', 'max:255'],
            'phone' => ['present', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10'],
        ]);
        $companies = Companies::find($request->input('company'));
        
        // dd($companies);

        $employees = new Employees();

        $employees->fname = $request->input('fname');
        $employees->lname = $request->input('lname');
        $employees->email = $request->input('email');
        $employees->phone = $request->input('phone');
        
        $companies->employees()->save($employees);

        return redirect()->back()->with('mssg', 'Create successfull!');
    }

    public function viewIndex($id) {

        $employe = Employees::findOrFail($id);
        return view('employees.view', [
            'employe' => $employe
        ]);
    }

    public function updateIndex($id) {

        $employe = Employees::findOrFail($id);
        $companies = Companies::all();
        return view('employees.update', [
            'employe' => $employe,
            'companies'=> $companies
        ]);
    }

    public function update(Request $request, $id) {

        $validatedData = $request->validate([
            'fname'   =>  'required',
            'lname'   =>  'required',
            'company' => 'present',
            'email' => 'present',
            'phone' => 'present'
        ]);
    
        $companies = Companies::find($request->input('company'));
        
        // dd($companies);

        $employees = Employees::findOrFail($id);

        $employees->fname = $request->input('fname');
        $employees->lname = $request->input('lname');
        $employees->company = $request->input('company');
        $employees->email = $request->input('email');
        $employees->phone = $request->input('phone');

        $employees->save();
        
        // $companies->employees()->save($employees);

        return redirect()->back()->with('mssg', 'Create successfull!');
    }

    public function delete($id) {

        $employe = Employees::findOrFail($id);

        $employe->delete();

        return redirect()->back()->with('alert', 'Company deleted!');
    }
}
