<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{

    protected $table = 'companies';

    protected $primaryKey = 'name';

    protected $keyType = 'string';
    
    protected $fillable = ['name', 'email', 'logo', 'website'];

    public $incrementing = false;

    public function getImageAttribute() {
        return $this->logo;
    }

    public function employees() {
        return $this->hasMany(Employees::class, 'company', 'name');
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
}
