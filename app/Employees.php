<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    
    protected $fillable = ['fname', 'lname', 'company', 'email', 'phone'];
    

    public function companies() {
        return $this->belongsTo(Companies::class, 'name', 'company');
    }
}


