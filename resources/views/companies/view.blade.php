@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="col-12 text-center">
            <h2>{{ $company->name }}</h2>
        </div>
        <div class="row">
            <div class="col-2">
                <h4>{{ __('messages.logo') }}: </h4>
            </div>
            <div class="col-10">
                <img src="{{ asset($company->logo) }}" alt="" width="100" height="100">
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-2">
                <h4>{{ __('messages.website') }}: </h4>
            </div>
            <div class="col-10">
                <h5><a href="#">{{ $company->website }}</a></h5>
            </div>
        </div>
        <div class="mt-5">
            <a class="btn btn-outline-primary" href="{{ url('/home/companies') }}">Back</a>
        </div>
    </div>

@endsection