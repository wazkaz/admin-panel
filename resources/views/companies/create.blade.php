@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>{{ __('messages.add_company') }}</h1>
        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif
        <form action="/home/companies/create/add" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">{{ __('messages.name') }}: </label>
                <input class="form-control" type="text" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="email">{{ __('messages.email') }}: </label>
                <input class="form-control" type="text" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="website">{{ __('messages.website') }}: </label>
                <input class="form-control" type="text" name="website" id="website">
            </div>
            <div class="form-group">
                <label for="logo">{{ __('messages.logo') }}: </label>
                <input class="form-control-file" type="file" name="logo" id="logo"/>
            </div>
            <input class="btn btn-outline-success" type="submit" value="{{ __('messages.create') }}">
            <a class="btn btn-outline-danger" href="{{ url('/home/companies') }}">{{ __('messages.back') }}</a>
        </form>
        <p class="success-mssg">{{ session('mssg') }}</p>
    </div>

@endsection