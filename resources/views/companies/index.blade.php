@extends('layouts.app')\

@section('content')

<div class="container">
    <div class="row">
        <div class="menu col-3">
            <h4>{{ __('messages.menu_c') }}</h4>
            <ul>
                <li><a href="{{ url('/home/companies') }}">{{ __('messages.overview') }}</a></li>
                <li><a href="{{ url('/home/companies/create') }}">{{ __('messages.add_company') }}</a></li>
                <li><a href="{{ url('/home/employees') }}">{{ __('messages.employees') }}</a></li>
            </ul>
        </div>
        <div class="col-9">
            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif
            <table class="display" id="Table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('messages.logo') }}</th>
                        <th>{{ __('messages.name') }}</th>
                        <th>{{ __('messages.email') }}</th>
                        <th>{{ __('messages.website') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td><img src="{{ asset($company->logo) }}" alt="logo" width="30" height="30"></td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->email }}</td>
                        <td>{{ $company->website }}</td>
                        <td class="d-flex justify-content-end">
                            <a class="btn btn-primary mr-2" href="/home/companies/view/{{ $company->name }}"><i class="far fa-eye"></i></a>
                            <a class="btn btn-success mr-2" href="/home/companies/update/{{ $company->name }}"><i class="far fa-edit"></i></a>
                            <a class="btn btn-danger mr-2" href="/home/comapnies/delete/{{ $company->name }}"><i class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>

@endsection