@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Update company informations</h2>
        <form action="/home/companies/update/{{  $company->name }}/modify" method="POST" role="form" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <label for="name">Name: </label>
                <input class="form-control" type="text" name="name" id="name" value="{{ $company->name }}">
            </div>
            <div class="form-group">
                <label for="email">Email: </label>
                <input class="form-control" type="text" name="email" id="email" value="{{ $company->email }}">
            </div>
            <div class="form-group">
                <label for="name">Website: </label>
                <input class="form-control" type="text" name="website" id="website" value="{{ $company->website }}">
            </div>
            <div class="form-group">
                <label for="logo">Logo: </label>
                <input class="form-control" type="file" name="logo" id="logo"/>
            </div>
            <input class="btn btn-outline-success" type="submit" value="Update">
            <a class="btn btn-outline-danger" href="{{ url('/home/companies') }}">Back</a>
        </form>
        <p class="success-mssg">{{ session('mssg') }}</p>
    </div>

@endsection