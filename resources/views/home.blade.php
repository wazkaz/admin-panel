@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('messages.dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>{{ __('messages.logged') }}</p>
                    <ul style="list-style:none;">
                        <li>
                            <a href="{{ url('/home/companies') }}" class="btn btn-primary mb-2">{{ __('messages.companies') }}</a><br>
                        </li>
                        <li>
                            <a href="{{ url('/home/employees') }}" class="btn btn-primary">{{ __('messages.employees') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
