@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Update employe informations</h2>
        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif
        <form action="/home/employees/update/{{ $employe->id }}/modify" method="POST" role="form">
        @csrf
            <div class="form-group">
                <label for="fname">First Name: </label>
                <input class="form-control" type="text" name="fname" id="fname" value="{{ $employe->fname }}"/>
            </div>
            <div class="form-group">
                <label for="lname">Last Name: </label>
                <input class="form-control" type="text" name="lname" id="lname" value="{{ $employe->lname }}"/>
            </div>
            <div class="form-group">
                <label for="email">Email: </label>
                <input class="form-control" type="text" name="email" id="email" value="{{ $employe->email }}"/>
            </div>
            <div class="form-group">
                <label for="company">Company: </label>
                <select name="company" class="form-control">
                    <option selected>{{ $employe->company }}</option>
                    @foreach($companies as $company)
                        <option id="company">{{ $company->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="phone">Phone number: </label>
                <input class="form-control" type="tel" name="phone" id="phone" value="{{ $employe->phone }}"/>
            </div>
            <input class="btn btn-outline-success" type="submit" value="Update">
            <a class="btn btn-outline-danger" href="{{ url('/home/employees') }}">Back</a>
        </form>
        <p class="success-mssg">{{ session('mssg') }}</p>
    </div>

@endsection