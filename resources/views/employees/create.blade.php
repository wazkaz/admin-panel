@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>{{ __('messages.add_employee') }}</h1>
                @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif
        <form action="/home/employees/create/add" method="POST" role="form">
            @csrf
            <div class="form-group">
                <label for="fname">{{ __('messages.first_name') }}: </label>
                <input class="form-control" type="text" name="fname" id="fname">
            </div>
            <div class="form-group">
                <label for="lname">{{ __('messages.last_name') }}: </label>
                <input class="form-control" type="text" name="lname" id="lname">
            </div>
            <div class="form-group">
                <label for="company">{{ __('messages.company') }}: </label>
                <select name="company" class="form-control">
                    <option selected>{{ __('messages.choose') }}</option>
                    @foreach($compnanies as $company)
                        <option id="company">{{ $company->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="email">{{ __('messages.email') }}: </label>
                <input class="form-control" type="text" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="phone">{{ __('messages.phone') }}: </label>
                <input class="form-control" type="tel" name="phone" id="phone"/>
            </div>
            <input class="btn btn-outline-success" type="submit" value="{{ __('messages.create') }}">
            <a class="btn btn-outline-danger" href="{{ url('/home/employees') }}">{{ __('messages.back') }}</a>
        </form>
        <p class="success-mssg">{{ session('mssg') }}</p>
    </div>

@endsection