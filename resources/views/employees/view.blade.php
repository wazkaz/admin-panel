@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row mb-3">
            <h2>{{ __('messages.details') }} {{ $employe->fname }} {{ $employe->lname }}</h2>
        </div>
        <div class="row mb-3">
            <h4><b>{{ __('messages.email') }}: </b>{{ $employe->email }}</h4>
        </div>
        <div class="row mb-3">
            <h4><b>{{ __('messages.phone') }}: </b>{{ $employe->phone }}</h4>
        </div>
        <div class="row mb-3">
            <h4><b>{{ __('messages.company') }}: </b>{{ $employe->company }}</h4>
        </div>
        <a class="btn btn-outline-primary" href="{{ url('/home/employees') }}">{{ __('messages.back') }}</a>
    </div>

@endsection