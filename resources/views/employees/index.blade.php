@extends('layouts.app')\

@section('content')

<div class="container">
    <div class="row">
        <div class="menu col-2">
            <h4>{{ __('messages.menu_e') }}</h4>
            <ul>
                <li><a href="{{ url('/home/employees') }}">{{ __('messages.overview') }}</a></li>
                <li><a href="{{ url('/home/employees/create') }}">{{ __('messages.add_employee') }}</a></li>
                <li><a href="{{ url('/home/companies') }}">{{ __('messages.companies') }}</a></li>
            </ul>
        </div>
        <div class="col-10">
            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif
            <table class="display" id="Table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('messages.first_name') }}</th>
                        <th>{{ __('messages.last_name') }}</th>
                        <th>{{ __('messages.company') }}</th>
                        <th>{{ __('messages.email') }}</th>
                        <th>{{ __('messages.phone') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($employees as $employe)
                    <tr>
                        <td>{{ $employe->id }}</td>
                        <td>{{ $employe->fname }}</td>
                        <td>{{ $employe->lname }}</td>
                        <td>{{ $employe->company }}</td>
                        <td>{{ $employe->email }}</td>
                        <td>{{ $employe->phone }}</td>
                        <td class="d-flex justify-content-end">
                            <a class="btn btn-primary mr-2" href="/home/employees/view/{{ $employe->id }}"><i class="far fa-eye"></i></a>
                            <a class="btn btn-success mr-2" href="/home/employees/update/{{ $employe->id }}"><i class="far fa-edit"></i></a>
                            <a class="btn btn-danger mr-2" href="/home/employees/delete/{{ $employe->id }}"><i class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection