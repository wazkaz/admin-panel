@component('mail::message')
# Greetings

New company {{ $data["name"] }} added.


Thanks,<br>
{{ config('app.name') }}
@endcomponent
