<?php
return [
    'welcome' => 'Welcome to our application',
    'home' => 'Home',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'email' => 'E-mail',
    'password' => 'Password',
    'remember' => 'Remember me',
    'forgot' => 'Forgot Your Password?',
    'name' => 'Name',
    'confirm' => 'Confirm Password',
    'dashboard' => 'Dashboard',
    'logged' => 'You are logged in!',
    'companies' => 'Companies',
    'employees' => 'Employees',
    'hungarian' => 'Hungarian',
    'english' => 'English',

    // companies
    'menu_c' => 'Companies Menu',
    'overview' => 'Overview',
    'add_company' => 'Add new comapany',
    'employees' => 'Employees',
    'logo' => 'Logo',
    'website' => 'Website',
    'back' => 'Back',
    'create' => 'Create',

    // employees
    'menu_e' => 'Employees Menu',
    'add_employee' => 'Add new employee',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'company' => 'Company',
    'phone' => 'Phone',
    'choose' => 'Choose...',
    'details' => 'Details for',
];